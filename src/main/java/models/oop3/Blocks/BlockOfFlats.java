package models.oop3.Blocks;

public class BlockOfFlats extends Block {

    public BlockOfFlats(boolean isInCity, int rooms, int numberOfPeople) {
        super(isInCity, rooms, numberOfPeople);
    }

    @Override
    public String printTypeOfBuilding() {
        return "This is a " + this.getClass().getSimpleName() +
                " and is a type of " + super.getClass().getSuperclass().getSimpleName() + ".";
    }

    @Override
    public double calculateWaterConsumptionPerMonth() {
        return 20 * this.getNumberOfPeople();
    }



}
