package models.oop3.Blocks;

import models.oop3.Building;

public abstract class Block extends Building {

    private int rooms;
    private int numberOfPeople;

    public Block(boolean isInCity, int rooms, int numberOfPeople) {
        super(isInCity);
        this.setRooms(rooms);
        this.setNumberOfPeople(numberOfPeople);
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        if (rooms <= 0) {
            throw new IllegalArgumentException("Your house needs to have at least one room!");
        }
        this.rooms = rooms;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        if (numberOfPeople < 0) {
            throw new IllegalArgumentException("The number " +
                    "of people living in the room, cannot be a negative one!");
        }
        this.numberOfPeople = numberOfPeople;
    }

    public abstract double calculateWaterConsumptionPerMonth();

}
