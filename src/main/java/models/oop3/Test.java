package models.oop3;

import models.oop3.Blocks.BlockOfFlats;
import models.oop3.Houses.Bungalow;
import models.oop3.PublicBuildings.Hospital;

public class Test {

    public static void main(String[] args) {

        try {
            BlockOfFlats block = new BlockOfFlats(true, 64, 32);
            System.out.println(block.printTypeOfBuilding());
            System.out.println("The consumption of water per month is: " + block.calculateWaterConsumptionPerMonth());

            System.out.println();

            Bungalow bungalow = new Bungalow(false, 5, 4, 1, 100);
            System.out.println(bungalow.printTypeOfBuilding());
            System.out.println("There are " + bungalow.getNumberOfPeople() + " people living here.");
            System.out.println(block.calculateWaterConsumptionPerMonth());
            System.out.println(bungalow.calculateLandTax());

            System.out.println();

            Hospital hospital = new Hospital(true);
            System.out.println(hospital.printTypeOfBuilding());


        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }

    }

}
