package models.oop3.PublicBuildings;

import models.oop3.Building;

public abstract class PublicBuilding extends Building {

    public PublicBuilding(boolean isInCity) {
        super(isInCity);
    }
}
