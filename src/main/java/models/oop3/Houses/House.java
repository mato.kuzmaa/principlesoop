package models.oop3.Houses;

import models.oop3.Building;

public abstract class House extends Building {

    private int rooms;
    private int numberOfPeople;
    private int floors;
    private double area;

    public House(boolean isInCity, int rooms, int numberOfPeople, int floors, double area) {
        super(isInCity);
        this.setRooms(rooms);
        this.setNumberOfPeople(numberOfPeople);
        this.floors = floors;
        this.area = area;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        if(rooms <= 0) {
            throw new IllegalArgumentException("Your house needs to have at least one room!");
        }
        this.rooms = rooms;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        if(numberOfPeople < 0) {
            throw new IllegalArgumentException("Number of people living " +
                    "in the room, cannot be a negative one!");
        }
        this.numberOfPeople=numberOfPeople;

    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        if(floors < 0) {
            throw new IllegalArgumentException("The house needs to have" +
                    "at least one floor!");
        }
        this.floors=floors;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        if(area <= 0) {
            throw new IllegalArgumentException("You need to correct" +
                    "the area you entered");
        }
        this.area = area;
    }

    public abstract double calculateLandTax();
}
















