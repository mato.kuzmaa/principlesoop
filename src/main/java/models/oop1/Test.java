package models.oop1;

public class Test {

    public static void main(String[] args) {

        try {

            Student jozko = new Student("Jozko", 33, 4, "Kudlovska");
            Student pavol = new Student("Pavol", "pavol@gmail.com", 8, "Stanicna");
            Student andrej = new Student("Andrej", 9, "Laborecka");
            Student michal = new Student("Michal", 54, "michal@gmail.com", 9, "Kukucinova");

            System.out.println(jozko);
            System.out.println(pavol);
            System.out.println(andrej);
            System.out.println(michal);

        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }
    }
}
