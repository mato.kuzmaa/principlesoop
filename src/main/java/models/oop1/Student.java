package models.oop1;

public class Student extends Person {

    private int schoolClass;
    private String school;



    public Student(String name, int age, String email, int schoolClass, String school) {
        super(name, age, email);
        this.setSchoolClass(schoolClass);
        this.setSchool(school);
    }

    public Student(String name, int age, int schoolClass, String school) {
        this(name, age, "no email provided", schoolClass, school);
    }

    public Student(String name, int schoolClass, String school) {
        this(name, -1, "no email provided", schoolClass, school);
    }

    public Student(String name, String email, int schoolClass, String school) {
        this(name, -1, email, schoolClass, school);
    }

    public int getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(int schoolClass) {
        if((schoolClass<1) || (schoolClass>9)) {
            throw new IllegalArgumentException("The class must be in the range from 1 to 9!");
        }
        this.schoolClass = schoolClass;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        if(school.length()<5) {
            throw new IllegalArgumentException("Check the name of the school!");
        }
        this.school = school;
    }

    @Override
    public String toString() {
        return "Student " +
                "name: " + this.getName() +
                ", age: " + this.getAge() +
                ", email: " + this.getEmail() +
                ", schoolClass: " + this.getSchoolClass() +
                ", school: " + this.getSchool();
    }
}
