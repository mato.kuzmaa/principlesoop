package models.oop2;

public class MultiSeedsBread extends Bread{

    protected static final double priceItem = 1.2;

    public MultiSeedsBread(String name, int amount) {
        super(name, amount);
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        if(!(name.equals("MB"))) {
            throw new IllegalArgumentException("You didn't enter your type of bread correctly!");
        }
        super.setName(name);
    }

    @Override
    public double calculateTotalPrice() {
        return this.getAmount()*priceItem;
    }
}
