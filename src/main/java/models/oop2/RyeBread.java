package models.oop2;

public class RyeBread extends Bread {

    protected static final double priceItem = 1.3;

    public RyeBread(String name, int amount) {
        super(name, amount);
    }

    public String getName() {
        return super.getName();
    }

    public void setName(String name) {
        if(!(name.equals("RB"))) {
            throw new IllegalArgumentException("You didn't enter your type of bread correctly!");
        }
        super.setName(name);
    }


    @Override
    public double calculateTotalPrice() {
        return this.getAmount()*priceItem;
    }
}
