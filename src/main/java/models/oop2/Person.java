package models.oop2;

public class Person {

    private String nameOfBread;
    private double money;

    public Person(String nameOfBread, double money) {
        this.setNameOfBread(nameOfBread);
        this.setMoney(money);
    }

    public String getNameOfBread() {
        return nameOfBread;
    }

    public void setNameOfBread(String nameOfBread) {
        this.nameOfBread = nameOfBread;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        if(money <= 0) {
            throw new IllegalArgumentException("You dont have enough money!");
        }
        this.money = money;
    }

    public int calculatePossibleAmountBread() {

        int possibleBuyings = 0;

        switch (this.getNameOfBread()) {
            case "RB":
                possibleBuyings = (int)(this.getMoney()/RyeBread.priceItem);
                break;
            case "BB":
                possibleBuyings = (int)(this.getMoney()/BrownBread.priceItem);
                break;
            case "MB":
                possibleBuyings = (int)(this.getMoney()/MultiSeedsBread.priceItem);
                break;
            default:
                throw new IllegalArgumentException("We don't sell such " +
                        "type of bread");
        }

        return possibleBuyings;
    }
}























