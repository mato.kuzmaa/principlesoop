package models.oop2;

public class Test {
    public static void main(String[] args) {

        RyeBread bread1 = new RyeBread("RB", 4);
        System.out.println("Rye bread: " + bread1.calculateTotalPrice());

        BrownBread bread3 = new BrownBread("BB", 4);
        System.out.println("Brown bread: " + bread3.calculateTotalPrice());

        MultiSeedsBread bread2 = new MultiSeedsBread("MB", 10);
        System.out.println("Multiseeds Bread: " + bread2.calculateTotalPrice());

        System.out.println("Total earnings: " + (bread1.calculateTotalPrice() +
                bread2.calculateTotalPrice() + bread3.calculateTotalPrice()));

        Person person = new Person("RB", 22);
        System.out.println("You may buy " + person.calculatePossibleAmountBread() +
                " loaves of " + person.getNameOfBread() + ".");

    }
}
